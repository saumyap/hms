package com.hms.handler.billing;

import com.hms.data.PatientBilling;
import com.hms.data.repository.BillingRepository;
import com.hms.menu.MenuHandler;

import static com.hms.utils.InputScanner.*;

public class PharmacyBillingHandler implements MenuHandler {
    private BillingRepository br = BillingRepository.instance();

    @Override
    public void handle() {
        String medicine = input("Name of medicine");
        double price = inputDouble("Enter price: ");
        int quantity = inputInt("Enter quantity: ");

        PatientBilling pb = new PatientBilling(br.getPatientId(), medicine, price, quantity, br.getMultiplier());
        br.insert(pb);
    }
}
