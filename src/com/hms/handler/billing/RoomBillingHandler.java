package com.hms.handler.billing;

import com.hms.data.PatientBilling;
import com.hms.data.repository.BillingRepository;
import com.hms.menu.MenuHandler;

import static com.hms.utils.InputScanner.input;
import static com.hms.utils.InputScanner.inputInt;

public class RoomBillingHandler implements MenuHandler {

    public static final double ROOM_RATE_PER_DAY = 500.00;
    private BillingRepository br = BillingRepository.instance();

    @Override
    public void handle() {
        String typeOfRoom = input("Type of Room - Shared, Private, Deluxe (S, P, D): ");
        int numberOfDays = inputInt("Number of Days: ");

        String roomDescription = "";
        switch(typeOfRoom.toUpperCase()) {
            case "S" : br.setMultiplier(1.0);
                        roomDescription = "Shared Room";
                       break;

            case "P" : br.setMultiplier(1.5);
                        roomDescription = "Private Room";
                       break;

            case "D" : br.setMultiplier(3.0);
                        roomDescription = "Deluxe Room";
                       break;
        }

        PatientBilling pb = new PatientBilling(br.getPatientId(),roomDescription,ROOM_RATE_PER_DAY, numberOfDays, br.getMultiplier());
        br.insert(pb);
    }
}
