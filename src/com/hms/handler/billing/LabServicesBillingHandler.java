package com.hms.handler.billing;

import com.hms.data.PatientBilling;
import com.hms.data.repository.BillingRepository;
import com.hms.menu.MenuHandler;

import static com.hms.utils.InputScanner.*;

public class LabServicesBillingHandler implements MenuHandler {
    private BillingRepository br = BillingRepository.instance();

    @Override
    public void handle() {
        System.out.println("Tests - MRI, ECG, EEG, BLOOD, XRAY, SURGERY");
        String test = input("Name of test: ");
        double rate = 0.0;
        String description = "";
        switch(test.toUpperCase()) {
            case "MRI" : rate = 3500;
                description = "Magnetic Resonance Imaging (MRI)";
                break;

            case "ECG" : rate = 1800;
                description = "Electrocardiogram (ECG)";
                break;

            case "EEG" : rate = 2200;
                description = "Electroencephalogram (EEG)";
                break;

            case "BLOOD" : rate = 800;
                description = "Blood Test";
                break;

            case "XRay" : rate = 500;
                description = "X-Ray";
                break;

            case "SURGERY" : rate = 15000;
                description = "Surgery";
                break;
            default :
                rate = inputDouble("Enter rate of "+test+": ");
                description = test;
                break;
        }

        PatientBilling pb = new PatientBilling(br.getPatientId(),description, rate, 1, br.getMultiplier());
        br.insert(pb);
    }
}
