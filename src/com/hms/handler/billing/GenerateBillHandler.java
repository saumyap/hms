package com.hms.handler.billing;

import com.hms.data.Patient;
import com.hms.data.PatientBilling;
import com.hms.data.repository.BillingRepository;
import com.hms.data.repository.PatientRepository;
import com.hms.menu.MenuHandler;

import java.io.IOException;

import static com.hms.utils.InputScanner.input;

public class GenerateBillHandler implements MenuHandler {
    private PatientRepository pr = PatientRepository.instance();
    private BillingRepository br = BillingRepository.instance();

    @Override
    public void handle() {

        Patient patient = pr.findById(br.getPatientId());
        PatientBilling[] pb = br.findByPatientId(br.getPatientId());

        System.out.println("Name: " + patient.getName());
        System.out.println("Age: " + patient.getAge() + " years");
        System.out.println(String.format("%s\t\t%s\t\t%s\t\t%s","ITEM","RATE","QUANTITY","COST"));
        System.out.println(String.format("%s\t\t%s\t\t%s\t\t%s","----","----","--------","----"));
        double totalBill = 0.0;
        for(int i = 0; i < pb.length; i++) {
            System.out.println(String.format("%s\t\t%.2f\t\t%d\t\t%.2f",pb[i].getItemDescription(),pb[i].getRate()*pb[i].getMultiplier(),pb[i].getQuantity(),pb[i].getPrice()));
            totalBill += pb[i].getPrice();
        }
        System.out.println(String.format("Total Bill: Rs. %.2f", totalBill));
        System.out.print("type enter to continue...");
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
