package com.hms.handler.billing;

import com.hms.data.repository.BillingRepository;
import com.hms.handler.BackHandler;
import com.hms.menu.MenuHandler;
import com.hms.menu.MenuItem;
import com.hms.menu.MenuServer;

import static com.hms.utils.InputScanner.input;

public class BillingHandler implements MenuHandler {
    private MenuItem[] menuItems = {
            new MenuItem("1. Room", '1', new RoomBillingHandler()),
            new MenuItem("2. Lab Services ", '2', new LabServicesBillingHandler()),
            new MenuItem("3. Pharmacy", '3', new PharmacyBillingHandler()),
            new MenuItem("4. Generate Bill", '4', new GenerateBillHandler()),
            new MenuItem("5. Back", '5', new BackHandler())
    };
    private MenuServer ms = new MenuServer();

    private BillingRepository br = BillingRepository.instance();

    @Override
    public void handle() {
        String patientId = input("Enter patient id:");
        br.setPatientId(patientId);
        this.ms.run(menuItems);
    }
}
