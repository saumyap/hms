package com.hms.handler.patients;

import com.hms.data.Patient;
import com.hms.data.repository.PatientRepository;
import com.hms.menu.MenuHandler;

public class ShowAllPatientsHandler implements MenuHandler  {
    private PatientRepository pr = PatientRepository.instance();
    @Override
    public void handle() {
        Patient[] p = pr.findAll();
        System.out.println(String.format("%s\t\t%s\t\t\t\t%s","ID","NAME","AGE"));
        System.out.println(String.format("%s\t\t%s\t\t\t\t%s","--","----","---"));
        for(int i = 0; i < p.length; i++) {
            System.out.println(String.format("%s\t\t%s\t\t%d",p[i].getId(),p[i].getName(),p[i].getAge()));
        }
    }
}
