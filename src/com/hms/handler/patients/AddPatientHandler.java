package com.hms.handler.patients;

import com.hms.data.Patient;
import com.hms.data.repository.PatientRepository;
import com.hms.utils.InputScanner;
import com.hms.menu.MenuHandler;

import static com.hms.utils.InputScanner.input;
import static com.hms.utils.InputScanner.inputInt;

public class AddPatientHandler implements MenuHandler {

    private PatientRepository pr = PatientRepository.instance();

    @Override
    public void handle() {

        System.out.println("Add a patient here.");

        String id = input("Enter patient id: ");
        String name = input("Enter patient name: ");
        int age = inputInt("Enter patient age: ");
        Patient p = new Patient(id, name, age);
        pr.insert(p);
    }
}
