package com.hms.handler.patients;

import com.hms.data.repository.PatientRepository;
import com.hms.utils.InputScanner;
import com.hms.menu.MenuHandler;

import static com.hms.utils.InputScanner.input;

public class RemovePatientHandler implements MenuHandler {
    private PatientRepository pr = PatientRepository.instance();
    @Override
    public void handle() {
        String id = input("Enter patient id to remove: ");
        pr.remove(id);
        System.out.println("Removed patient with id=" + id);
    }
}
