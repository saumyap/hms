package com.hms.handler.patients;

import com.hms.handler.BackHandler;
import com.hms.menu.MenuHandler;
import com.hms.menu.MenuItem;
import com.hms.menu.MenuServer;

public class PatientsHandler implements MenuHandler {

    private MenuItem[] menuItems = {
            new MenuItem("1. Add a Patient", '1', new AddPatientHandler()),
            new MenuItem("2. Find a Patient", '2', new FindPatientHandler()),
            new MenuItem("3. Remove a Patient", '3', new RemovePatientHandler()),
            new MenuItem("4. Show all Patients", '4', new ShowAllPatientsHandler()),
            new MenuItem("5. Back", '5', new BackHandler())
    };

    private MenuServer ms = new MenuServer();

    @Override
    public void handle() {
        ms.run(menuItems);
    }
}
