package com.hms.handler.patients;

import com.hms.data.Patient;
import com.hms.data.repository.PatientRepository;
import com.hms.utils.InputScanner;
import com.hms.menu.MenuHandler;

import static com.hms.utils.InputScanner.input;

public class FindPatientHandler implements MenuHandler {
    private PatientRepository pr = PatientRepository.instance();

    @Override
    public void handle() {

        System.out.println("Find a patient here.");

        String keyword = input("Keyword? ");
        Patient[] patients = pr.findByName(keyword);

        for(Patient p : patients) {
            System.out.println(p);
        }
    }
}
