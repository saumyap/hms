package com.hms.handler;

import com.hms.menu.MenuHandler;

public class ExitHandler implements MenuHandler {
    @Override
    public void handle() {

        System.out.println("Bye Bye.");
        System.exit(0);
    }
}
