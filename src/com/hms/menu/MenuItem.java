package com.hms.menu;

import java.util.UUID;

public class MenuItem {
    private char keyCode;
    private String description;
    private MenuHandler handler;

    public MenuItem(String description, char keyCode, MenuHandler handler) {
        this.description = description;
        this.keyCode = keyCode;
        this.handler = handler;
    }

    public char getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(char keyCode) {
        this.keyCode = keyCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MenuHandler getHandler() {
        return handler;
    }

    public void setHandler(MenuHandler handler) {
        this.handler = handler;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "keyCode=" + keyCode +
                ", description='" + description + '\'' +
                ", handler=" + handler +
                '}';
    }
}
