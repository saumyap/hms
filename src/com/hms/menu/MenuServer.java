package com.hms.menu;

import com.hms.handler.BackHandler;

import java.util.HashMap;
import java.util.Map;

import static com.hms.utils.InputScanner.input;

public class MenuServer {
    Map<String, MenuHandler> map = new HashMap<>();
    public void run(MenuItem[] items) {
        while(true) {
            for(int i = 0; i < items.length; i++) {
                System.out.println(items[i].getDescription());
                map.put(String.valueOf(items[i].getKeyCode()), items[i].getHandler());
            }
            String keyCode = input("What would you like to do? ");
            MenuHandler handler = map.get(keyCode);
            if(handler instanceof BackHandler) break;
            handler.handle();
        }
    }
}
