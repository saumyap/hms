package com.hms;

import com.hms.handler.ExitHandler;
import com.hms.handler.billing.BillingHandler;
import com.hms.handler.patients.PatientsHandler;
import com.hms.menu.MenuItem;
import com.hms.menu.MenuServer;

public class Main {

    private MenuItem[] menuItems = {
            new MenuItem("1. Patients", '1', new PatientsHandler()),
            new MenuItem("2. Billing", '2', new BillingHandler()),
            new MenuItem("3. Exit", '3', new ExitHandler())

    };

    public static void main(String[] args) {

        Main m = new Main();
        MenuServer ms = new MenuServer();
        ms.run(m.menuItems);
    }
}
