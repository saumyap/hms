package com.hms.data;

public class PatientBilling {
    String patientId;
    String itemDescription;
    double rate;
    int quantity;
    double multiplier;
    double price;

    public PatientBilling(String patientId, String itemDescription, double rate, int quantity, double multiplier) {
        this.patientId = patientId;
        this.itemDescription = itemDescription;
        this.rate = rate;
        this.quantity = quantity;
        this.multiplier = multiplier;
        this.price = this.quantity * this.rate * this.multiplier;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    public double getPrice() {
        return price;
    }
}
