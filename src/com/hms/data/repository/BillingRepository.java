package com.hms.data.repository;

import com.hms.data.Patient;
import com.hms.data.PatientBilling;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BillingRepository {

    private static final BillingRepository SINGLE_INSTANCE = new BillingRepository();
    private String patientId;
    private double multiplier = 1.0;
    private List<PatientBilling> billingList = new ArrayList<>();
    File file = new File("billing.dat");

    {
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.loadFromFile();
    }

    private BillingRepository(){}

    public static BillingRepository instance() {
        return SINGLE_INSTANCE;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;

        //adjust the multiplier
        for(PatientBilling pb : billingList) {
            if(pb.getPatientId().equals(patientId) && pb.getItemDescription().contains("Room")) {
                this.setMultiplier(pb.getMultiplier());
            }
        }
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    private void loadFromFile() {
        try {
            FileInputStream in = new FileInputStream(file);
            BufferedReader d = new BufferedReader(new InputStreamReader(in));
            for(String str = d.readLine(); str != null;str = d.readLine()) {
                String[] st =str.split(",");
                PatientBilling pb = new PatientBilling(st[0], st[1], Double.parseDouble(st[2]), Integer.parseInt(st[3]),Double.parseDouble(st[4]));
                this.billingList.add(pb);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveToFile() {
        PrintWriter out = null;
        try {
            FileOutputStream fout = new FileOutputStream(file);
            out = new PrintWriter(fout);
            for(PatientBilling pb : this.billingList) {
                out.println(String.format("%s,%s,%.2f,%d,%.1f", pb.getPatientId(), pb.getItemDescription(), pb.getRate(), pb.getQuantity(),pb.getMultiplier()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(out != null) {
                out.flush();
                out.close();
            }
        }
    }
    public void insert(PatientBilling pb) {
        this.billingList.add(pb);
        this.saveToFile();
    }

    public PatientBilling[] findByPatientId(String patientId) {

        List<PatientBilling> pbList = this.billingList.stream().filter(pb -> pb.getPatientId().equals(patientId)).collect(Collectors.toList());

        return pbList.toArray(new PatientBilling[0]);
    }
}
