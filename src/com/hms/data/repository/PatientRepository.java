package com.hms.data.repository;

import com.hms.data.Patient;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class PatientRepository {

    private static final PatientRepository SINGLE_INSTANCE = new PatientRepository();
    File file = new File("patients.dat");
    List<Patient> patients = new ArrayList<>();

    {
        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.loadFromFile();
    }

    private PatientRepository() {}

    public static PatientRepository instance() {
        return SINGLE_INSTANCE;
    }

    private void loadFromFile() {
        try {
            FileInputStream in = new FileInputStream(file);
            BufferedReader d = new BufferedReader(new InputStreamReader(in));
            for(String str = d.readLine(); str != null;str = d.readLine()) {
                String[] st =str.split(",");
                Patient pt = new Patient(st[0], st[1], Integer.parseInt(st[2]));
                this.patients.add(pt);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveToFile() {
        PrintWriter out = null;
        try {
            FileOutputStream fout = new FileOutputStream(file);
            out = new PrintWriter(fout);
            for(Patient p : this.patients) {
                out.println(String.format("%s,%s,%d", p.getId(), p.getName(), p.getAge()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(out != null) {
                out.flush();
                out.close();
            }
        }
    }

    public void insert(Patient p) {

        this.patients.add(p);
        this.saveToFile();
    }

    public Patient findById(String patientId) {

        Patient p = null;

        for(int i = 0; i < this.patients.size(); i++) {
            Patient tmp = patients.get(i);

            if(tmp.getId().equals(patientId)) {
                p = tmp;
                break;
            }
        }

        return p;
    }

    public Patient[] findByName(String keyword) {

        Patient[] p = new Patient[0];

    for(int i = 0 ; i < this.patients.size(); i++)
    {
        Patient temp = patients.get(i);
        if (temp.getName().equals(keyword))
        {
            p[i] = temp;
            break;
        }
    }
    return p;
    }


    public void remove(String patientId) {
        Patient p = this.findById(patientId);
        this.patients.remove(p);
        this.saveToFile();
    }


    public Patient[] findAll() {
        return patients.toArray(new Patient[0]);
    }
}
